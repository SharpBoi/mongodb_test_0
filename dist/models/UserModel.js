var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "mongoose"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const mongoose_1 = __importStar(require("mongoose"));
    let UserSchema = new mongoose_1.Schema({
        email: { type: String, required: true },
        Name: { type: String, required: true },
        Surname: { type: String, required: true }
    });
    let UserModel = mongoose_1.default.model("UserModel", UserSchema);
    exports.default = UserModel;
});
