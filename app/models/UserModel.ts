import mongoose, { Schema, Document } from "mongoose";

export interface IUserModel extends Document {
    email: string;
    Name: string;
    Surname: string;
}

let UserSchema = new Schema({
    email: { type: String, required: true },
    Name: { type: String, required: true },
    Surname: { type: String, required: true }
})

let UserModel = mongoose.model<IUserModel>("UserModel", UserSchema);

export default UserModel;